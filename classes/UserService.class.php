<?php

class UserService
{
    protected $_login;    // using protected so they can be accessed
    protected $_password; // and overidden if necessary

    protected $_db;       // stores the database handler
    protected $_user;     // stores the user data

    public function __construct(PDO $db, $login, $password)
    {
        $this->_db = $db;
        $this->_login = $login;
        $this->_password = $password;
    }

    public function login()
    {
        $user = $this->_checkCredentials();
        if ($user) {
            $this->_user = $user; // store it so it can be accessed later
            //$_SESSION['user_id'] = $user['id'];
            return true;
        }
        return false;
    }

    protected function _checkCredentials()
    {
        try {
            $stmt = $this->_db->prepare('SELECT * FROM limev3_users WHERE users_name=?');
            $stmt->execute(array($this->_login));
            if ($stmt->rowCount() > 0) {
                $user = $stmt->fetch(PDO::FETCH_ASSOC);
                if (password_verify($this->_password, $user['password'])) {
                    return $user;
                }
            }
            return false;
        } catch (PDOException $e) {
            echo '<pre>' . $e->getMessage() . '</pre>';
        }
    }

    public function getUser()
    {
        return $this->_user;
    }

    public function __destruct()
    {
        // Disconnect from DB
        $this->_db = null;
    }
}
