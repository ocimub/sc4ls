<?php

class SavedControlService
{
    /**
     *  @var int
     **/
    protected $_scid;         // identifiant saved control
    /**
     *  @var int
     **/
    protected $_sid;          // identifiant de l'enquête
    /**
     *  @var int
     **/
    protected $_srid;         // identifiant de la réponse
    /**
     *  @var string
     **/
    protected $_identifier;   // identifiant du répondant
    /**
     *  @var string
     **/
    protected $_access_code;  // mot de passe
    /**
     *  @var string
     **/
    protected $_email;        // email du répondant
    /**
     *  @var string
     **/
    protected $_ip;           // ip du répondant
    /**
     *  @var int
     **/
    protected $_saved_thisstep; // étape de la sauvegarde = lastpage
    /**
     * @var string
     */
    protected $_status;       // statut de la sauvegarde
    /**
     *  @var string
     **/
    protected $_saved_date;   // date de la sauvegarde
    /**
     *  @var string
     **/
    protected $_refurl;        // url de référence
    /**
     * @var PDO
     */
    protected $_db;           // gestionnaire de la bd
    /**
     *  @var int
     **/
    protected $_userid;       // identifiant de l'administrateur du questionnaire

    // le questionnaire existe-il ? 
    public function surveyExists()
    {
        $stmt = $this->_db->prepare(
            "SELECT * FROM limev3_surveys WHERE sid=:sid"
        );
        $stmt->bindParam("sid", $this->_sid, PDO::PARAM_INT);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $sc = $stmt->fetch(PDO::FETCH_ASSOC);
            return true;
        }
        return false;
    }

    // l'utilisateur connecté est-il autorisé à ajouter un saved control ? 
    public function hasPermissions()
    {
        //si admin on autorise
        if (intval($this->_userid) === 1) {
            return true;
        }

        $stmt = $this->_db->prepare(
            "SELECT * FROM limev3_permissions WHERE entity LIKE 'survey' 
                AND permission LIKE 'responses' 
                AND uid=:uid 
                AND entity_id=:entity_id"
        );
        $stmt->bindParam("uid", $this->_userid, PDO::PARAM_INT);
        $stmt->bindParam("entity_id", $this->_sid, PDO::PARAM_INT);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $sc = $stmt->fetch(PDO::FETCH_ASSOC);
            if (intval($sc['create_p']) === 1) {
                return true;
            }
        }
        return false;
    }

    // retourne un Saved Control basé sur l'identifiant questionnaire, l'identifiant réponse et l'email 
    public function get()
    {
        $stmt = $this->_db->prepare(
            "SELECT * FROM limev3_saved_control WHERE sid = :sid 
                AND srid = :srid 
                AND email LIKE :email"
        );
        $stmt->bindParam("sid", $this->_sid, PDO::PARAM_INT);
        $stmt->bindParam("srid", $this->_srid, PDO::PARAM_INT);
        $stmt->bindParam("email", $this->_email, PDO::PARAM_STR, 192);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $data;
        }
        return false;
    }

    // retourne tous les Saved Control basé sur l'identifiant questionnaire et l'identifiant réponse 
    public function getAll()
    {
        $sql = "SELECT * FROM limev3_saved_control WHERE sid = :sid ";

        if ($this->_srid) {
            $sql .= " AND srid = :srid";
        }

        $stmt = $this->_db->prepare($sql);
        $stmt->bindParam("sid", $this->_sid, PDO::PARAM_INT);

        if ($this->_srid) {
            $stmt->bindParam("srid", $this->_srid, PDO::PARAM_INT);
        }

        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $saved_controls = $stmt->fetchAll();
            $scArray = array();
            foreach ($saved_controls as $sc) {
                $scArray[] = $sc;
            }
            return $scArray;
        }
        return false;
    }

    /* Ajouter une nouvelle entrée */
    public function add()
    {
        $stmt = $this->_db->prepare("INSERT INTO limev3_saved_control(sid,srid,identifier,access_code,email,ip,saved_thisstep,status,saved_date,refurl) VALUES (:sid,:srid,:identifier,:access_code,:email,:ip,:saved_thisstep,:status,:saved_date,:refurl)");
        $stmt->bindParam("sid", $this->_sid, PDO::PARAM_INT);
        $stmt->bindParam("srid", $this->_srid, PDO::PARAM_INT);
        $stmt->bindParam("identifier", $this->_identifier, PDO::PARAM_STR);
        $stmt->bindParam("access_code", $this->_access_code, PDO::PARAM_STR);
        $stmt->bindParam("email", $this->_email, PDO::PARAM_STR);
        $stmt->bindParam("ip", $this->_ip, PDO::PARAM_STR);
        $stmt->bindParam("saved_thisstep", $this->_saved_thisstep, PDO::PARAM_STR);
        $stmt->bindParam("status", $this->_status, PDO::PARAM_STR);
        $stmt->bindParam("saved_date", $this->_saved_date, PDO::PARAM_STR);
        $stmt->bindParam("refurl", $this->_refurl, PDO::PARAM_STR);
        $stmt->execute();
        return true;
    }

    /* Supprime une entrée */
    public function del()
    {
        $stmt = $this->_db->prepare("DELETE FROM limev3_saved_control WHERE scid = :scid");
        $stmt->bindParam("scid", $this->_scid, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->rowCount();
    }

    /* Change le code d'accès */
    public function updatePassword()
    {
        $stmt = $this->_db->prepare("UPDATE limev3_saved_control SET access_code = :access_code WHERE scid = :scid");
        $stmt->bindParam("access_code", $this->_access_code, PDO::PARAM_STR);
        $stmt->bindParam("scid", $this->_scid, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function getDb()
    {
        return $this->_db;
    }

    public function setDb(PDO $value)
    {
        $this->_db = $value;
    }

    public function getUserId()
    {
        return $this->_userid;
    }

    public function setUserId($value)
    {
        $this->_userid = $value;
    }

    public function getScid()
    {
        return $this->_scid;
    }

    public function setScid($value)
    {
        $this->_scid = $value;
    }

    public function getSid()
    {
        return $this->_sid;
    }

    public function setSid($value)
    {
        $this->_sid = $value;
    }

    public function getSrid()
    {
        return $this->_srid;
    }

    public function setSrid($value)
    {
        $this->_srid = $value;
    }

    public function getIdentifier()
    {
        return $this->_identifier;
    }

    public function setIdentifier($value)
    {
        $this->_identifier = $value;
    }

    public function getAccessCode()
    {
        return $this->_access_code;
    }

    public function setAccessCode($value)
    {
        $this->_access_code = $value;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setEmail($value)
    {
        $this->_email = $value;
    }

    public function getIp()
    {
        return $this->_ip;
    }

    public function setIp($value)
    {
        $this->_ip = $value;
    }

    public function getSavedThisstep()
    {
        return $this->_saved_thisstep;
    }

    public function setSavedThisstep($value)
    {
        $this->_saved_thisstep = $value;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function setStatus($value)
    {
        $this->_status = $value;
    }

    public function getSavedDate()
    {
        return $this->_saved_date;
    }

    public function setSavedDate($value)
    {
        $this->_saved_date = $value;
    }

    public function getRefurl()
    {
        return $this->_refurl;
    }

    public function setRefurl($value)
    {
        $this->_refurl = $value;
    }

    public function __destruct()
    {
        // Disconnect from DB
        $this->_db = null;
    }

    public function getInstance($data)
    {
        $this->setScid($data['scid']);
        $this->setSid($data['sid']);
        $this->setSrid($data['srid']);
        $this->setIdentifier($data['identifier']);
        $this->setAccessCode($data['access_code']);
        $this->setEmail($data['email']);
        $this->setIp($data['ip']);
        $this->setSavedThisstep($data['saved_thisstep']);
        $this->setStatus($data['status']);
        $this->setSavedDate($data['saved_date']);
        $this->setRefurl($data['refurl']);
        return $this;
    }
}
