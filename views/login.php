<?php
if (!defined('LOADER')) exit('No direct script access allowed');

require_once './classes/UserService.class.php';

include 'header.inc.php';

if (isset($_POST['login'])) {

    $username = !empty($_POST['username']) ? trim($_POST['username']) : null;
    $passwordAttempt = !empty($_POST['password']) ? trim($_POST['password']) : null;

    $userService = new UserService(getDB(), $username, $passwordAttempt);

    if (!$userService->login()) {
        $error_msg = "Identifiant et/ou mot de passe incorrect";
    } else {
        $user = $userService->getUser();
        $_SESSION['user_id'] = $user['uid'];
        $_SESSION['user_name'] =  $user['full_name'];

        header("Location: //" . $_SERVER['HTTP_HOST'] . strtok($_SERVER["REQUEST_URI"], '?'));
        exit;
    }
}

if ($error_msg) {
    echo '<div class="mt-1 w-50 alert alert-danger" role="alert">' . $error_msg . '</div>';
}

?>

<form method="post">
    <div class="form-group">
        <label for="username">Identifiant</label>
        <input type="text" class="form-control w-50" id="username" name="username" required>
    </div>
    <div class="form-group">
        <label for="password">Mot de passe</label>
        <input type="password" class="form-control w-50" id="password" name="password" required>
    </div>
    <button type="submit" name="login" class="btn btn-primary">S'identifier</button>
</form>