<?php
if (!defined('LOADER')) exit('No direct script access allowed');

require_once './classes/SavedControlService.class.php';

include_once 'header.inc.php';

$process = true;
$error_msg = "";
$success_msg = "";
$warn_msg = "";

if (isset($_POST['save']) && isset($_SESSION['user_id'])) {

    $uid = $_SESSION['user_id'];
    $sid = !empty($_POST['sid']) ? trim($_POST['sid']) : null;
    $srid = !empty($_POST['srid']) ? trim($_POST['srid']) : null;
    $identifier = !empty($_POST['identifier']) ? trim($_POST['identifier']) : null;
    $access_code = !empty($_POST['access_code']) ? trim($_POST['access_code']) : null;
    $email = !empty($_POST['email']) ? trim($_POST['email']) : null;
    $saved_thistep = !empty($_POST['saved_thistep']) ? trim($_POST['saved_thistep']) : null;

    $sc = new SavedControlService();

    $sc->setDb(getDB());
    $sc->setUserId($uid);
    $sc->setSid($sid);
    $sc->setSrid($srid);
    $sc->setIdentifier($identifier);
    $sc->setAccessCode(hash('sha256', $access_code));
    $sc->setEmail($email);
    $sc->setIp($_SERVER['REMOTE_ADDR']);
    $sc->setSavedThisstep($saved_thistep);
    $sc->setStatus("S");
    $sc->setSavedDate(date("Y-m-d H:i:s"));
    $sc->setRefurl($_SERVER['HTTP_REFERER'] . "&userid=" . $uid);

    while ($process) {
        try {
            if (!$sc->surveyExists()) {
                $warn_msg = "Le questionnaire n° " . $sid . " n'existe pas.";
                $process = false;
                break;
            }
            if (!$sc->hasPermissions()) {
                $warn_msg = "Vous n'avez pas les permissions sur le questionnaire n° " . $sid . ".";
                $process = false;
                break;
            }
            if ($sc->get()) {
                $warn_msg = "Un enregistrement pour la réponse n° " . $srid . " avec l'email " . $email . " existe déjà.";
                $process = false;
            }
        } catch (PDOException $e) {
            $error_msg = $e->getMessage();
            $warn_msg = "Données non sauvegardées.";
            $process = false;
        }
        break;
    }

    if ($process) {
/*         echo '<div class="alert alert-warning" role="alert">VERSION TEST : les données ne sont pas enregistrées en base de données.</div>';
        $success_msg = "Données sauvegardées.";
        echo "<pre>";
        print_r($sc);
        echo "</pre>"; */
        try {
            if ($sc->add()) {
                $success_msg = "Données sauvegardées.";
            } else {
                $warn_msg = "Données non sauvegardées.";
            }
        } catch (PDOException $e) {
            $error_msg = $e->getMessage();
            $warn_msg = "Données non sauvegardées.";
        }
    } else {
        $warn_msg .= " Données non sauvegardées.";
    }

    if ($success_msg) {
        echo '<div class="alert alert-success" role="alert">' . $success_msg . '</div>';
    }

    if ($error_msg) {
        echo '<div class="alert alert-danger" role="alert">' . $error_msg . '</div>';
    }

    if ($warn_msg) {
        echo '<div class="alert alert-warning" role="alert">' . $warn_msg . '</div>';
    }
}


if (isset($_SESSION['user_name']) && isset($_SESSION['user_id'])) {
?>
    <h1>Ajouter un nouvel enregistrement</h1>
    <form method="post">
        <div class="form-group">
            <label for="sid">Identifiant du questionnaire <i class="material-icons md-18" data-toggle="tooltip" title="Assurez-vous d'avoir la permission d'ajouter des réponses dans ce questionnaire">info</i></label>
            <input type="number" class="form-control" id="sid" name="sid" required>
        </div>
        <div class="form-group">
            <label for="srid">Identifiant de la réponse <i class="material-icons md-18" data-toggle="tooltip" title="Correspond à la colonne id des réponses">info</i></label>
            <input type="number" class="form-control" id="srid" name="srid" required>
        </div>
        <div class="form-group">
            <label for="identifier">Identifiant du répondant <i class="material-icons md-18" data-toggle="tooltip" title="Créez un identifiant sans espace ou caractères spéciaux">info</i></label>
            <input type="text" class="form-control" id="identifier" name="identifier" required>
        </div>
        <div class="form-group">
            <label for="access_code">Mot de passe</label>
            <input type="text" class="form-control" id="access_code" name="access_code" required>
        </div>
        <div class="form-group">
            <label for="email">Email du répondant</label>
            <input type="email" class="form-control" id="email" name="email" required>
        </div>
        <div class="form-group">
            <label for="saved_thistep">Dernière page sauvegardée / lastpage</label>
            <input type="number" class="form-control" id="saved_thistep" name="saved_thistep" required>
        </div>
        <button type="submit" name="save" class="btn btn-primary">Enregistrer</button>
    </form>
<?php } ?>