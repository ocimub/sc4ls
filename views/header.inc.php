<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="./" title="Gestion des accès aux réponses sauvegardées pour les questionnaires limesurvey">Sc4ls</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item"><a class="nav-link" href="?page=addRecord">Ajouter</a></li>
            <li class="nav-item"><a class="nav-link" href="?page=editRecord">Rechercher/modifier</a></li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <?php if (!isset($_SESSION['user_name'])) { ?>
                <li class="nav-item"><a class="nav-link" href="?page=login">Connexion</a></li>
            <?php } else { ?>
                <li class="nav-item"><span class="navbar-text"><?php echo $_SESSION['user_name']; ?></span></li>
                <li class="nav-item"><a class="nav-link" href="?page=logout">Déconnexion</a></li>
            <?php } ?>
        </ul>
    </div>
</nav>