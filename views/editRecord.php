<?php
if (!defined('LOADER')) exit('No direct script access allowed');

require_once './classes/SavedControlService.class.php';

include_once 'header.inc.php';

if (isset($_SESSION['user_name']) && isset($_SESSION['user_id'])) {
?>
    <h1>Rechercher un enregistrement</h1>
    <form method="post">
        <div class="form-group">
            <label for="sid">Identifiant du questionnaire <i class="material-icons md-18" data-toggle="tooltip" title="Assurez-vous d'avoir la permission d'administrer ce questionnaire">info</i></label>
            <input type="number" class="form-control" id="sid" name="sid" required>
        </div>
        <div class="form-group">
            <label for="srid">Identifiant de la réponse <i class="material-icons md-18" data-toggle="tooltip" title="Correspond à la colonne id des réponses">info</i></label>
            <input type="number" class="form-control" id="srid" name="srid">
        </div>
        <button type="submit" name="search" class="btn btn-primary">Rechercher</button>
    </form>
    <?php }

if (isset($_POST['search']) && isset($_SESSION['user_id'])) {

    $uid = $_SESSION['user_id'];
    $sid = !empty($_POST['sid']) ? trim($_POST['sid']) : null;
    $srid = !empty($_POST['srid']) ? trim($_POST['srid']) : null;


    $sc = new SavedControlService();

    $sc->setDb(getDB());
    $sc->setSid($sid);
    $sc->setSrid($srid);

    if ($sc->getAll()) {
        $scs = $sc->getAll();
    ?>
        <table class="table my-2">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Id questionnaire</th>
                    <th scope="col">Id réponse</th>
                    <th scope="col">Identifiant</th>
                    <th scope="col">Email</th>
                    <th scope="col">Changer le code</th>
                    <th scope="col">Supprimer</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($scs as $sc) {
                ?>
                    <tr id="<?= $sc['scid']; ?>">
                        <th scope="row"><?= $sc['scid']; ?></th>
                        <td><?= $sc['sid']; ?></td>
                        <td><?= $sc['srid']; ?></td>
                        <td><?= $sc['identifier']; ?></td>
                        <td><?= $sc['email']; ?></td>
                        <td><button type="button" class="btn material-icons access-code" data-toggle="modal" data-target="#accessCodeModal" data-scid="<?= $sc['scid']; ?>" data-sid="<?= $sc['sid']; ?>">vpn_key</button></td>
                        <td><button type="button" class="btn material-icons red delete" data-scid="<?= $sc['scid']; ?>" data-sid="<?= $sc['sid']; ?>">delete</button></td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
<?php
    } else {
        echo "0 enregistrement";
    }
}
?>

<!-- Modal Access code -->
<div class="modal fade" id="accessCodeModal" tabindex="-1" role="dialog" aria-labelledby="accessCodeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="accessCodeModalLabel">Nouveau code d'accès</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-success d-none" role="alert"></div>
                <div class="alert alert-danger d-none" role="alert"></div>
                <form>
                    <div class="form-group">
                        <label for="access-code" class="col-form-label">Nouveau code:</label>
                        <input type="text" class="form-control" id="access-code-input" required>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                <button id="access-code-save-btn" type="button" class="btn btn-primary">Sauvegarder</button>
            </div>
        </div>
    </div>
</div>

<script src="./scripts/delete.js"></script>
<script src="./scripts/edit.js"></script>