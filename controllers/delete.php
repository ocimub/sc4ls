<?php
include 'header-ajax.inc.php';

if (isset($_POST['delete']) && isset($_SESSION['user_id'])) {

    $uid = $_SESSION['user_id'];
    $scid = !empty($_POST['scid']) ? trim($_POST['scid']) : null;
    $sid = !empty($_POST['sid']) ? trim($_POST['sid']) : null;

    $sc = new SavedControlService();
    $sc->setDb(getDB());
    $sc->setUserId($uid);
    $sc->setSid($sid);
    $sc->setScid($scid);

    try {
        if (!$sc->hasPermissions()) {
            echo "Vous n'avez pas les permissions sur le questionnaire n° " . $sid . ".";
            exit;
        }
        echo $sc->del();//1 si ok ou 0 sinon
    } catch (PDOException $e) {
        echo $e->getMessage();
        exit;
    }
} else {
    echo "Données non sauvegardées.";
}
