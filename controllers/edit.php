<?php
include 'header-ajax.inc.php';;

if (isset($_POST['edit']) && isset($_SESSION['user_id'])) {

    $uid = $_SESSION['user_id'];
    $scid = !empty($_POST['scid']) ? trim($_POST['scid']) : null;
    $sid = !empty($_POST['sid']) ? trim($_POST['sid']) : null;
    $access_code = !empty($_POST['code']) ? trim($_POST['code']) : null;

    $sc = new SavedControlService();
    $sc->setDb(getDB());
    $sc->setUserId($uid);
    $sc->setSid($sid);
    $sc->setScid($scid);
    $sc->setAccessCode(hash('sha256', $access_code));

    try {
        if (!$sc->hasPermissions()) {
            echo "Vous n'avez pas les permissions sur le questionnaire n° " . $sid . ".";
            exit;
        }
        echo $sc->updatePassword();//1 si ok ou 0 sinon
    } catch (PDOException $e) {
        echo $e->getMessage();
        exit;
    }
} else {
    echo "Données non sauvegardées.";
}
