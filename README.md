# Scals

Saved controls for Limesurvey : gestion des accès aux réponses sauvegardées pour les questionnaires limesurvey.

## Pré-requis

Avoir une installation fonctionnelle de Limesurvey sur le même serveur.

## Usage

Renseignez la constante LIMECONFIG_PATH dans le fichier config.php avec le chemin d'accès du fichier de configuration de votre installation de limesurvey.
