<?php
session_start();
define("LOADER", "");

try {
    if (!file_exists('config.php'))
        throw new Exception('Le fichier config.php n\'est pas configuré.');
    else
        require_once('config.php');
} catch (Exception $e) {
    exit($e->getMessage());
}

//include 'views/header.inc.php';

if (isset($_GET['page'])) {

    $page = htmlspecialchars($_GET['page']);
    $show_warning_session = true;

    switch($page) {
        case "login":
            include 'views/login.php';
            $show_warning_session = false;
            break;
        case "addRecord":
            include 'views/addRecord.php';
            break;
        case "editRecord":
            include 'views/editRecord.php';
            break;
        case "logout":
            session_destroy();
            header("Location: //" . $_SERVER['HTTP_HOST'] . strtok($_SERVER["REQUEST_URI"], '?'));
        default:
            $show_warning_session = false;
            echo "Cette page n'existe pas";
        
    }

} else {
    include 'views/header.inc.php';
    echo "Gestion des accès aux réponses sauvegardées pour les questionnaires limesurvey.";
}

if (!isset($_SESSION['user_name']) && $show_warning_session) {
    echo '<div class="alert alert-warning" role="alert">Vous devez être connecté pour utiliser ce service</div>';
}