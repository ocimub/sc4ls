$(document).ready(function () {
    $('button.delete').click(function () {
        var scid = $(this).data('scid');
        var sid = $(this).data('sid');

        $.ajax({
            url:"controllers/delete.php",
            method: "POST",
            data: {delete: true, scid: scid, sid: sid},
            success:function(response){
                if (response == 1 ){
                    var el = $("#"+scid);
                    el.css('background','pink');
                    el.fadeOut(2000,function(){
                        el.remove();
                    });
                } else {
                    alert(response);
                }
            }
        });
    });
});