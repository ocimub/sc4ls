$(document).ready(function () {
    var modal = $('#accessCodeModal');
    var saveBtn = $('#access-code-save-btn');

    modal.on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var scid = button.data('scid');
        var sid = button.data('sid');
        saveBtn.click(function (e) {
            e.preventDefault();
            var newCode = modal.find('#access-code-input').val();
            if (newCode === "") {
                modal.find('.alert-danger').removeClass('d-none').text('Vous devez saisir un code valide');
            } else {
                $.ajax({
                    url: "controllers/edit.php",
                    method: "POST",
                    data: {
                        edit: true,
                        scid: scid,
                        sid: sid,
                        code: newCode
                    },
                    success: function (response) {
                        if (response == 1) {
                            modal.find('.alert-danger').addClass('d-none');
                            modal.find('.alert-success').removeClass('d-none').text("Données sauvegardées");//message de succès
                            saveBtn.detach();//enlève le bouton de sauvegarde
                        } else {
                            modal.find('.alert-danger').removeClass('d-none').text(response);//affiche les erreurs
                        }
                    }
                });
            }
        });
    });

    //réinitialisation quand le modal est caché
    modal.on('hidden.bs.modal', function () {
        modal.find('.alert').addClass('d-none');//masque les div alert
        modal.find('form').trigger('reset');//reset le formulaire
        modal.find('.modal-footer').append(saveBtn);//ajoute le bouton de sauvegarde
        saveBtn.off('click');//supprime le handler
    })

});